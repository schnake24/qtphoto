#include "loadimage.h"

#include <QImageReader>

#define MAX_IMAGE_MEM 512 // in MB

LoadImage::LoadImage(int w, int h, const QString &filename, QObject *parent) : QThread(parent), m_filename(filename), m_width(w), m_height(h)
{
}

void LoadImage::run()
{
    QImageReader imageReader(m_filename);
    imageReader.setAllocationLimit(MAX_IMAGE_MEM);
    imageReader.setAutoTransform(true);

    QImage image = imageReader.read();
    if( !image.isNull() ) {
        //qInfo() << "Loaded image infos" << image;
        QImage scaled = image.scaled(m_width, m_height, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        //qInfo() << "Scaled image infos" << scaled;
        emit imageLoaded(scaled);
    } else {
        emit imageLoaded(QImage());
    }
}
