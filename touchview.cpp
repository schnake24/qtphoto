#include "touchview.h"

#include <QEvent>
#include <QDebug>
#include <QList>
#include <QPushButton>
#include <QGraphicsProxyWidget>

TouchView::TouchView(QWidget *parent) : QGraphicsView(parent)
{
}

TouchView::~TouchView()
{
}

void TouchView::mousePressEvent(QMouseEvent* ev)
{
    /*
    qDebug() << "Mouse press";
    qDebug() << "Local position:" << ev->pos();
    qDebug() << "Global position:" << ev->globalPos();
    qDebug() << "Buttons:" << ev->buttons();
    qDebug() << "";
    */
    //qDebug() << "Mouse event:" << ev;

    QGraphicsItem *item = itemAt(ev->pos());
    if(item->type() == QGraphicsPixmapItem::Type) {
        qDebug() << "In background";
        emit onMousePressed();
    }

    QGraphicsView::mousePressEvent(ev);
}

void TouchView::mouseMoveEvent(QMouseEvent* ev)
{
    /*
    qDebug() << "Mouse move";
    qDebug() << "Local position:" << ev->pos();
    qDebug() << "Global position:" << ev->globalPos();
    qDebug() << "Buttons:" << ev->buttons();
    qDebug() << "";
    */
    QGraphicsView::mouseMoveEvent(ev);
}

void TouchView::mouseReleaseEvent(QMouseEvent* ev)
{
    /*
    qDebug() << "Mouse release";
    qDebug() << "Local position:" << ev->pos();
    qDebug() << "Global position:" << ev->globalPos();
    qDebug() << "Buttons:" << ev->buttons();
    qDebug() << "";
    */
    QGraphicsView::mouseReleaseEvent(ev);
}

void TouchView::mouseDoubleClickEvent(QMouseEvent *event)
{
    QGraphicsView::mouseDoubleClickEvent(event);
}
