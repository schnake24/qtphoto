#ifndef TOUCHVIEW_H
#define TOUCHVIEW_H

#include <QGraphicsView>
#include <QGestureEvent>

class TouchView : public QGraphicsView
{
    Q_OBJECT

public:
    TouchView(QWidget *parent = nullptr);
    virtual ~TouchView();

protected:
    virtual void mouseDoubleClickEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent* ev);
    virtual void mouseMoveEvent(QMouseEvent* ev);
    virtual void mouseReleaseEvent(QMouseEvent* ev);

signals:
    void onMousePressed();
};

#endif // TOUCHVIEW_H
