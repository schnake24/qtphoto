#include "widget.h"
#include "ui_widget.h"

#include <QtDebug>
#include <QRandomGenerator>
#include <QGraphicsProxyWidget>
#include <QGuiApplication>
#include <QScreen>
#include <QImageReader>
#include <QProcess>
#include <loadimage.h>

#include "clickpixmapitem.h"

Widget::Widget(const QString &path, int timeoutTime, QWidget *parent) : QWidget(parent), ui(new Ui::Widget)
{
    ui->setupUi(this);

#ifdef QT_NO_DEBUG
    QRect rect = QGuiApplication::primaryScreen()->geometry();
    m_width = rect.width();
    m_height = rect.height();
#else
    m_width = size().width();
    m_height = size().height();
#endif
    m_scene = new TouchScene();
    m_scene->setBackgroundBrush(Qt::black);
    m_scene->setSceneRect(0, 0, m_width, m_height);
    ui->gv->setScene(m_scene);

    m_pixmapCurrentItem = m_pixmapNextItem = nullptr;

    m_timeoutTime = timeoutTime;
    connect(&timer, SIGNAL(timeout()), SLOT(onTimeout()));
    if(m_timeoutTime<1000) {
        m_timeoutTime = 15000;
    }
    qInfo() << "Start timer with timeout" << timeoutTime;
    timer.setSingleShot(true);
    timer.start(m_timeoutTime);

    m_widgetOffTimer.setInterval(5000);
    m_widgetOffTimer.setSingleShot(true);
    connect(&m_widgetOffTimer, SIGNAL(timeout()), SLOT(hideButtons()));

    m_index = 0;
    m_dirWatcher = new DirWatcher(path, this);
    connect(m_dirWatcher, SIGNAL(filelistChanged(QStringList)), SLOT(filesChanged(QStringList)));
    m_dirWatcher->refresh();

    initNavigation();

    connect(ui->gv, SIGNAL(onMousePressed()), SLOT(onClicked()));
    loadScene();
}

Widget::~Widget()
{
    delete m_backButton;
    delete ui;
}

void Widget::initNavigation()
{
    m_backButton = new QPushButton();
    m_backButton->setGeometry(0, 0, m_width/4, m_height);
    m_backButton->setVisible(false);
    m_backButton->setText(tr("Zurück"));
    m_backButtonProxy = m_scene->addWidget(m_backButton);
    m_backButtonProxy->setZValue(1);

    m_shutdownButton = new QPushButton();
    m_shutdownButton->setGeometry(m_width/2 - 200, m_height - 100, 400, 100);
    m_shutdownButton->setVisible(false);
    m_shutdownButton->setText(tr("Ausschalten"));
    m_shutdownButtonProxy = m_scene->addWidget(m_shutdownButton);
    m_shutdownButtonProxy->setZValue(1);

    connect(m_backButton, SIGNAL(clicked()), SLOT(back()));
    connect(m_shutdownButton, SIGNAL(clicked()), SLOT(shutdown()));
}

void Widget::onClicked()
{
    if(isButtonsDisplayed()) {
        hideButtons();
    } else {
        showButtons();
    }
}

void Widget::showButtons()
{
    displayButtons(true);
    m_widgetOffTimer.start();
}

void Widget::hideButtons()
{
    displayButtons(false);
    m_widgetOffTimer.stop();
}

void Widget::displayButtons(bool show)
{
    m_backButton->setVisible(show);
    m_shutdownButton->setVisible(show);
}

bool Widget::isButtonsDisplayed()
{
    return m_backButton->isVisible();
}

void Widget::back()
{
    qDebug() << "Back";
    timer.stop();

    m_mutexIndex.lock();
    m_index -= 2;
    if(m_index < 0) {
        m_index += m_files.count();
    }
    m_mutexIndex.unlock();

    loadScene();
}

void Widget::shutdown()
{
#ifdef QT_NO_DEBUG
    QProcess process;
    qInfo() << "Shutdown system";

    QStringList args;
    args << "systemctl" << "poweroff";
    process.startDetached("sudo", args);

#else
    qDebug() << "Shutdown should be done";
    QCoreApplication::exit(0);
#endif
}

void Widget::onTimeout()
{
    qDebug() << "Timeout";

    loadScene();
}

const QString& Widget::nextImageFilename()
{
    m_mutexIndex.lock();
    ++m_index;
    //qDebug() << "Index " << m_index;
    while(m_index>=m_files.count() || m_index<0) {
        if(m_index>=m_files.count()) {
            m_index -= m_files.count();
        } else {
            m_index += m_files.count();
        }
    }
    m_mutexIndex.unlock();

    return m_files.at(m_index);
}

void Widget::loadScene()
{
    QString filename = nextImageFilename();
    qDebug() << "Load next image " << filename;
    LoadImage *loadImage = new LoadImage(m_width, m_height, filename, this);

    connect(loadImage, SIGNAL(imageLoaded(QImage)), SLOT(imageLoaded(QImage)));
    connect(loadImage, SIGNAL(finished()), loadImage, SLOT(deleteLater()));
    loadImage->start();
}

void Widget::imageLoaded(QImage scaledImage)
{
    qDebug() << "Image received from thread";
    if(!scaledImage.isNull()) {
        // Draw image on target size in center (prepared for blending)
        QPixmap pixmap(m_width, m_height);
        pixmap.fill(Qt::black);
        QPainter painter;
        painter.begin(&pixmap);
        painter.fillRect(0, 0, m_width, m_height, Qt::black);
        painter.drawImage((m_width - scaledImage.width()) / 2, (m_height - scaledImage.height()) / 2, scaledImage);
        painter.end();

        ClickPixmapItem *newItem = new ClickPixmapItem(pixmap);
        newItem->setPos(0, 0);
        newItem->setZValue(0);
        m_scene->addItem(newItem);

        if(m_pixmapCurrentItem) delete(m_pixmapCurrentItem);
        m_pixmapCurrentItem = newItem;
        timer.start(m_timeoutTime);
    } else {
        timer.start(0);
    }
}

void Widget::filesChanged(QStringList files)
{
    m_index = 0;
    m_files = files;
}
