#ifndef CLICKPIXMAPITEM_H
#define CLICKPIXMAPITEM_H

#include <QGraphicsPixmapItem>

class ClickPixmapItem : public QGraphicsPixmapItem
{
public:
    ClickPixmapItem(QGraphicsItem *parent = nullptr);
    ClickPixmapItem(QPixmap &pixmap, QGraphicsItem *parent = nullptr);
};

#endif // CLICKPIXMAPITEM_H
