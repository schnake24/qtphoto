#ifndef DIRWATCHER_H
#define DIRWATCHER_H

#include <QObject>
#include <QString>
#include <QDir>

class QFileSystemWatcher;

class DirWatcher : public QObject
{
    Q_OBJECT
public:
    DirWatcher(const QString &dirPath, QObject *parent = nullptr);
    virtual ~DirWatcher();

    QStringList files() const;

public slots:
    void refresh();

protected:
    QStringList m_files;
    QString m_dirPath;
    QDir m_dir;
    QFileSystemWatcher *m_filesystemWatcher;

signals:
    void filelistChanged(const QStringList &files);
};

#endif // DIRWATCHER_H
