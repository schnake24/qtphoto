#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QPushButton>
#include <QMutex>

#include "dirwatcher.h"
#include "touchscene.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(const QString &path, int timeoutTime, QWidget *parent = nullptr);
    ~Widget();

    TouchScene *m_scene;
    QGraphicsPixmapItem *m_pixmapCurrentItem;
    QGraphicsPixmapItem *m_pixmapNextItem;

    void loadScene();

private:
    Ui::Widget *ui;

    int m_timeoutTime;
    QTimer timer;
    QTimer m_widgetOffTimer;

    int m_width, m_height;

    int m_index;
    QStringList m_files;
    DirWatcher *m_dirWatcher;

    QPushButton *m_backButton;
    QPushButton *m_shutdownButton;
    QGraphicsProxyWidget *m_backButtonProxy;
    QGraphicsProxyWidget *m_shutdownButtonProxy;

    QMutex m_mutexIndex;

    void initNavigation();
    const QString& nextImageFilename();

    bool isButtonsDisplayed();

public slots:
    void back();
    void shutdown();

private slots:
    void showButtons();
    void hideButtons();
    void displayButtons(bool show);

    void imageLoaded(QImage scaledImage);

    void onTimeout();
    void filesChanged(QStringList files);

    void onClicked();
};
#endif // WIDGET_H
