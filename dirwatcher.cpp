#include "dirwatcher.h"

#include <QFileSystemWatcher>
#include <QRandomGenerator>

DirWatcher::DirWatcher(const QString &dirPath, QObject *parent) : QObject(parent), m_dirPath(dirPath)
{
    QStringList nameFilters;
    nameFilters << "*.jpg" << "*.jpeg" << "*.png" << "*.gif";
    m_dir.setPath(m_dirPath);
    m_dir.setFilter(QDir::Files);
    m_dir.setNameFilters(nameFilters);

    m_filesystemWatcher = new QFileSystemWatcher();
    m_filesystemWatcher->addPath(m_dirPath);

    refresh();

    connect(m_filesystemWatcher, SIGNAL(directoryChanged(QString)), SLOT(refresh()));
}

DirWatcher::~DirWatcher()
{
    delete m_filesystemWatcher;
}

void DirWatcher::refresh()
{
    m_dir.refresh();
    QStringList files =  m_dir.entryList();

    // Fill list randomly
    QStringList newList;
    while(!files.isEmpty()) {
        int i = QRandomGenerator::global()->bounded(files.count());
        newList.append(m_dir.filePath(files.at(i)));
        files.removeAt(i);
    }
    m_files = newList;

    emit filelistChanged(m_files);
}

QStringList DirWatcher::files() const
{
    return m_files;
}
