#include "widget.h"

#include <Qt>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setDoubleClickInterval(400);

    //QCursor cursor(Qt::BlankCursor);
#ifdef QT_NO_DEBUG
    a.setOverrideCursor(Qt::BlankCursor);
    a.changeOverrideCursor(Qt::BlankCursor);
#endif

    QString time = argv[2];
    Widget w(argv[1], time.toInt());
    w.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
#ifndef QT_NO_DEBUG
    w.show();
#else
    w.showFullScreen();
#endif

    return a.exec();
}
