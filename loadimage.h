#ifndef LOADIMAGE_H
#define LOADIMAGE_H

#include <QObject>
#include <QImage>
#include <QThread>

class LoadImage : public QThread
{
    Q_OBJECT

public:
    explicit LoadImage(int w, int h, const QString &filename, QObject *parent = nullptr);

protected:
    void run() override;

private:
    QString m_filename;
    int m_width, m_height;

signals:
    void imageLoaded(QImage image);
};

#endif // LOADIMAGE_H
